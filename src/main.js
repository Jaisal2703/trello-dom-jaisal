async function apiFetcher(url, req_type) {
  var fetchedResult = await fetch(url, {
    method: req_type
  });
  return fetchedResult.json();
}

// Function to display Cards present in list

async function displayCardsFunction() {
  const url =
    'https://api.trello.com/1/lists/5e0f20365cb81104e37bfad6/cards?key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1';
  var result = await apiFetcher(url, 'get');
  result.forEach(element => {
    createDiv(element.name, element.id);
  });
}

displayCardsFunction(); // To display cards on screen continue we have to call the function directly

// Function to create a DIV tag in html to add a new card in the list
function createDiv(input, id) {
  var div = document.createElement('div');
  div.id = id;
  div.className = 'cards';
  div.addEventListener('click', function() {
    clrChklst();
    document.querySelector(
      '.modal-footer'
    ).innerHTML = `<input type="text" id="chklst-input" placeholder="Add Checklist" />
    <button class='insert-checklist' id=${id}>Add Checklist</button>
    <button
    onclick="clrChklst()"
      type="button"
      class="btn btn-default"
      data-dismiss="modal"
    >
      Exit
    </button>`;
    displayChecklist(div.id, input);
  });
  div.setAttribute('data-target', '#myModal');
  div.setAttribute('data-toggle', 'modal');
  div.innerHTML = `<h5>${input}</h5> <input type="button" class = "dlt-button" value="×" onclick="removeCard(this)" />`;
  document.getElementById('card-container').appendChild(div);
}

// Function to delete a card from the list

async function removeCard(dltDiv) {
  event.stopPropagation();
  const url = `https://api.trello.com/1/cards/${dltDiv.parentNode.id}?key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`;
  await apiFetcher(url, 'delete');
  document.getElementById('card-container').removeChild(dltDiv.parentNode);
}

// Function which will get invoke once user clicks on Add Card Button

async function addCard() {
  const input = document.getElementById('text-input').value;
  if (input.length === 0 || input.trim().length === 0) {
    alert('Enter a Valid Card Name');
  } else {
    const url = `https://api.trello.com/1/cards?name=${input}&idList=5e0f20365cb81104e37bfad6&keepFromSource=all&key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`;
    var result = await apiFetcher(url, 'post');
    createDiv(input, result.id);
  }
}

//Function to add a Checklist in card

async function addCheckList(id) {
  const chklstName = document.getElementById('chklst-input').value;
  if (chklstName.length === 0 || chklstName.trim().length === 0) {
    alert('Enter a Valid Checklist Name');
  } else {
    const url = `https://api.trello.com/1/checklists?idCard=${id}&name=${chklstName}&key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`;
    var chklst_result = await apiFetcher(url, 'post');
    createChklst(chklstName, chklst_result.id);
  }
}

// Function to create a DIV tag to add a checklist

function createChklst(name, id) {
  var chklst_div = document.createElement('div');
  chklst_div.id = id;
  chklst_div.className = 'CheckList';
  chklst_div.innerHTML = `<h6>${name}</h6><input type="button" class ="dlt-chklst-button dlt-button" value="×" onclick="removeChklist(this)"/>
  <div class="chckitems-body" id=${id}>
    </div>
    <div class="chckitems-footer" id=${id}>
      <input type="text" id=${id} class="itemss"placeholder="Create New Items">
      <button id=${id} onclick="addItem(id)">Add Items</button>
    </div>
  </div>`;
  document.getElementById('modal-card-content').appendChild(chklst_div);
}

// Function to add a Check Item in Checklist

function addItem(divdetails) {
  const namee = document.querySelector('.itemss').value;

  const id = divdetails;

  const input = document.querySelector(`input[id="${id}"]`).value;
  const url = `https://api.trello.com/1/checklists/${id}/checkItems?name=${input}&pos=bottom&checked=false&key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`;
  var result = apiFetcher(url, 'post');
  createCheckItems(input, result.id, id);
}

// Function to create a DIV tag to add the check item into Checklist

function createCheckItems(name, checkItemId, chcklstId) {
  var div = document.createElement('div');
  div.id = checkItemId;

  div.className = 'checkitems';
  div.innerHTML = `<input type="checkbox" class="list${checkItemId}" id=${checkItemId}/> <label for=${checkItemId}>${name}</label>`;

  document.getElementById(`${chcklstId}`).appendChild(div);
}

// Function to display all the check items in the checklist when it is open

async function displaycheckItems(chcklstId, card_id) {
  const url = `https://api.trello.com/1/checklists/${chcklstId}/checkItems?key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`;
  const result = await apiFetcher(url, 'get');
  result.forEach(ele => {
    createCheckItems(ele.name, ele.id, ele.idChecklist);
    if (ele.state === 'complete')
      document.querySelector(`.list${ele.id}`).checked = true;
    else document.querySelector(`.list${ele.id}`).checked = false;
    document
      .querySelector(`.list${ele.id}`)
      .addEventListener('click', function() {
        checkItems(
          ele.id,
          card_id,
          document.querySelector(`.list${ele.id}`).checked
        );
      });
  });
}

// Function to display the checklist once the modal is invoke

async function displayChecklist(id, crdnName) {
  document.getElementById(
    'checklist-header'
  ).innerHTML = `<h2>${crdnName}</h2>`;
  const url = `https://api.trello.com/1/cards/${id}/checklists?checkItems=all&checkItem_fields=name%2CnameData%2Cpos%2Cstate&filter=all&fields=all&key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`;

  var showChklst_result = await apiFetcher(url, 'get');
  showChklst_result.forEach(element => {
    createChklst(element.name, element.id);
    displaycheckItems(element.id, id);
  });

  document
    .querySelector('.insert-checklist')
    .addEventListener('click', function() {
      addCheckList(id);
    });
}

// displayChecklist();

// Function to remove a checklist from a card

async function removeChklist(rmChklst) {
  const url = `https://api.trello.com/1/checklists/${rmChklst.parentNode.id}?key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`;
  await apiFetcher(url, 'delete');
  document
    .getElementById('modal-card-content')
    .removeChild(rmChklst.parentNode);
}

// Function to render the screen and display the checklist properly

function clrChklst() {
  var alldiv = document.getElementById('modal-card-content');
  alldiv.innerHTML = '';
}

// Function to check and uncheck an check item

function checkItems(itemId, cardId, state) {
  if (state == true) {
    state = 'complete';
  } else {
    state = 'incomplete';
  }
  fetch(
    `https://api.trello.com/1/cards/${cardId}/checkItem/${itemId}?state=${state}&key=6b3f8447fcbcae69f0a8b179cf2d6cad&token=cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1`,
    { method: 'PUT' }
  );
}
